import { Component, OnInit } from '@angular/core';
import { Router, ActivationEnd, ResolveStart, NavigationStart } from '@angular/router';
import { trigger, transition, query, style, group, animate, animateChild } from '@angular/animations';


import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UserService } from './services/user.service';
import { CustomReuseStrategy } from './app.module';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('routerAnimation', [
      transition('* => fowards', [
        query(':leave', [
          style({ transform: 'translate3d(0,0,0)', zIndex: 1, opacity: 1 })
        ], { optional: true }),
        query(':enter', [
          style({ transform: 'translate3d(0,40%,0)', zIndex: 2, opacity: 0 })
        ], { optional: true }),
        group([
          query(':enter', [
            animate('200ms 300ms ease-out', style({ transform: 'translate3d(0,0,0)', opacity: 1 }))
          ], { optional: true })
        ])
      ]),
      transition('* => back', [
        query(':leave', [
          style({ transform: 'translate3d(0,0,0)', zIndex: 2, opacity: 1 })
        ], { optional: true }),
        query(':enter', [
          style({ transform: 'translate3d(0,0,0)', zIndex: 1, opacity: 1 })
        ], { optional: true }),
        group([
          query(':leave', [
            animate('200ms 300ms ease-out', style({ transform: 'translate3d(0,40%,0)', opacity: 0 }))
          ], { optional: true })
        ])
      ])
    ])
  ]
})
export class AppComponent implements OnInit {
  public loader = true;
  public animationValue = 'fowards';
  private routes = [];

  constructor(public router: Router, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private userService: UserService) {

  }

  preventRoutes(url) {
    var path = url.split('/')[1];
    if (this.userService.isLogged) {
      if (path != 'private') {
        this.router.navigate(['/private/home']);
      }
    } else {
      CustomReuseStrategy.clear();
      if (path != 'public') {
        this.router.navigate(['/public/main']);
      }
    }
  }

  getRouteAnimation(outlet) {
    return this.animationValue;
  }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.statusBar.styleBlackTranslucent();
      this.statusBar.backgroundColorByHexString('#2969e4');
      this.userService.resolve().then(() => {
        this.preventRoutes(this.router.url);
        this.loader = false;
        const item = localStorage.getItem('routes');
        if (item) {
          this.routes = JSON.parse(item);
        }
        this.router
          .events
          .subscribe((event) => {
            if (event instanceof NavigationStart) {
              this.preventRoutes(event.url);
            }
    
            if (event instanceof ResolveStart) {
    
              const last = this.routes[this.routes.length - 1];
              if (last && last == event.url) {
                this.routes.splice(-1, 2);
              }
              const older = this.routes[this.routes.length - 2];
              this.animationValue = 'fowards';
              if (older && older == event.url) {
                this.routes.splice(-2, 2);
                this.animationValue = 'back';
              }
              this.routes.push(event.url);
              localStorage.setItem('routes', JSON.stringify(this.routes));
            }
          });
        setTimeout(() => {
          this.splashScreen.hide();
        }, 200);
      });
    });
  }

}
