import { Component, OnInit, HostBinding } from '@angular/core';
import { Router, Scroll } from '@angular/router';
import { ModalService } from '../../services/modal.service';
import { UserService } from '../../services/user.service';
import { FiltersService } from '../../services/filters.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  public scope;
  public component: string;
  private customer_type = null;
  public customer_types = [];
  public customer_categories = [];
  public customer_subcategory = [];
  public customer_cache = [];


  public forms = {
    user_edit: {
      valid: false,
      disabled: false,
      values: {}
    },
    filter_discover: {
      valid: false,
      disabled: true,
      values: {}
    }
  }




  @HostBinding('class.open') public open = false;

  constructor(private router: Router, private modalService: ModalService, private userService: UserService, private filtersService: FiltersService) {
    this.router.events.subscribe((val) => {
      if (val instanceof Scroll) {
        if (val.anchor) {
          this.component = val.anchor;
          this.forms.filter_discover.values = this.filtersService.get('discover') || {};
          this.scope = this.modalService.scope;

          setTimeout(() => {
            this.open = true;
          });
        } else {
          this.open = false;
        }
      }
    });
  }

  ngOnInit() {
    (async () => {
      this.customer_types = await this.populate('/customer-types');
      var response = await this.userService.request('/cache-intervals', 'get');
      response.data.data.forEach(element => {
        this.customer_cache.push({
          text: element.interval,
          value: element.id
        });
      });
    })().then(() => {
      this.forms.filter_discover.disabled = false;
    }).catch(() => {

    });
  }

  async populate(url) {
    const response = await this.userService.request(url, 'get');
    const arr = [];
    response.data.data.forEach(element => {
      arr.push({
        text: element.nome,
        value: element.id
      });
    });
    return arr;
  }

  changeType(val) {
    this.customer_categories = [];
    this.customer_subcategory = [];
    this.customer_type = val;
    if (!val) return;
    (async () => {
      this.customer_categories = await this.populate(`/customer-types/${val}/categories`);

    })().catch(() => {

    });
  }

  saveFilters(data) {
    this.filtersService.set('discover', data);
    this.close();
  }

  changeCategory(val) {
    this.customer_subcategory = [];
    if (!val) return;
    (async () => {
      this.customer_subcategory = await this.populate(`/customer-types/${this.customer_type}/categories/${val}/subcategories`);
    })().catch(() => {

    });
  }

  close() {
    history.back();
  }

  select(item){
    setTimeout(()=>{
      this.close();
      this.scope.select(item);
    },150);
  }

  updateProfile(data) {
    this.forms.user_edit.disabled = true;
    this.userService.edit(data).then(()=>{
      history.back();
      if(this.scope && this.scope.onUpdtate)this.scope.onUpdtate();
      this.forms.user_edit.disabled = false;

    });
  }

}
