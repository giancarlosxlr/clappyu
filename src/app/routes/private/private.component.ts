import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../services/menu.service';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-private',
  templateUrl: '/./private.component.html',
  styleUrls: ['./private.component.scss']
})
export class PrivateComponent implements OnInit {

  public active = true;
  public data = null;
  public unread = 0;

  public items = [
    {
      name: 'início',
      url: '/private/home',
      notifications: 0,
    },
    {
      name: 'discover',
      url: '/private/discover',
      notifications: 0,
    },
    {
      name: 'agenda',
      url: '/private/agenda',
      notifications: 0,
    },
    {
      name: 'conversas',
      url: '/private/talks',
      notifications: 0,
    },
    {
      name: 'eventos',
      url: '/private/events',
      notifications: 0,
    },
    {
      name: 'favoritos',
      url: '/private/fav',
      notifications: 0,
    },
    {
      name: 'feedback',
      url: '/private/feedback',
      notifications: 0,
    },
    {
      name: 'parceiros',
      url: '/private/partners',
      notifications: 0,
    },
    {
      name: 'ajuda',
      url: '/private/help',
      notifications: 0,
    }
  ];

  constructor(public menuService: MenuService , private router: Router, private userService: UserService, private messageService: MessageService) {
    this.data = this.userService.data;
    this.userService.change.subscribe(()=>{
      this.data = this.userService.data;
    });

    this.items.find((item)=>{return item.name == 'conversas'}).notifications = this.messageService.chats.reduce((accumulator, currentValue) => {return accumulator + currentValue.unread}, 0);
    this.messageService.change.subscribe(()=>{
      this.items.find((item)=>{return item.name == 'conversas'}).notifications = this.messageService.chats.reduce((accumulator, currentValue) => {return accumulator + currentValue.unread}, 0);
    });
  }

  ngOnInit() {
    
  }
  
  logout(){
    this.menuService.close();
    this.userService.logout().then( ()=>{
      this.router.navigate(['/public/main']);
    });
  }

  goTo(link){
    this.menuService.close();
    setTimeout(()=>{
      this.router.navigate([link]);
    },200);
  }
}
