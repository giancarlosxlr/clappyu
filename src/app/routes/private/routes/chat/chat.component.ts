import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from '../../../../services/message.service';
import { ModalService } from '../../../../services/modal.service';
import { Chat } from '../../../../classes/chat.class';
import { Message } from '../../../../classes/message.class';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  public user;
  public chat: Chat;
  public msg = '';
  private _fileInput;
  public loader = false;
  private scroll;
  constructor(private userService: UserService, private route: ActivatedRoute, private messageService: MessageService, private modalService: ModalService) {

    this.route.params.subscribe((data: any) => {
      this.chat = this.messageService.getChat(data.token);
      if (!this.chat) {
        history.back();
        return;
      }
    });

  }

  @ViewChild('scroll') set setScroll(elm){
    if(!elm)return;
    this.scroll = elm.nativeElement;
    this.scroll.addEventListener('scroll', ()=>{
      if(!this.loader){
        if(this.scroll.scrollTop < 10){
          this.loader = true;
          const oldSize = this.scroll.scrollHeight;



          
          this.messageService.loadMessages(this.chat, this.chat.messages.length + 20).then(()=>{
            setTimeout(()=>{
              const top = this.scroll.scrollHeight - oldSize;
              this.scroll.scrollTop = top;
              this.loader = false;
            },200);
          });
        }
      }
    });
  };

  @ViewChild('filesInput') set filesInput(elm){
    this._fileInput = elm.nativeElement;
    this._fileInput.addEventListener('change', () => {
      (async () => {
        for(let i=0;i<this._fileInput.files.length;i++){
          await this.messageService.sendMessage(this.chat.token, null, this._fileInput.files[i]);
        }
      })().then(() => {
        this.toBottom();
      }).catch((e) => {
        console.log(e);
      });



    });
  };


  ngOnInit() {
    this.toBottom();
  }


  get token() {
    return this.userService.data.perfil.token;
  }

  sendMessage(msg, attachment?) {
    (async () => {
      await this.messageService.sendMessage(this.chat.token, msg, attachment);
    })().then(() => {

      this.toBottom();
    }).catch((e) => {
      console.log(e);
    });

  }

  toBottom(){
    setTimeout(()=>{
      if(!this.scroll){
        this.toBottom();
        return;
      }
      this.scroll.scrollTop = this.scroll.scrollHeight;
    },200);
  }

  openAttachments() {
    this.modalService.open('attachments', {
      select: (type) => {
        switch(type){
          case 'file' : this._fileInput.click();
        }
      }
    });
  }

  get names(){
    if(!this.chat)return '';
    if(this.chat.event){

    } else{
      const names = [];
      this.chat.participants.forEach((participant)=>{
          if(participant.username)names.push(participant.username);
      });
      return names.join(', ');
    }
  }

  get avatares(){
    if(!this.chat)return [];
    if(this.chat.event){

    } else{
      const avatares = [];
      this.chat.participants.forEach((participant, i)=>{
        if(i < 4){
          avatares.push(participant.avatar);
        }
      });
      return avatares;
    }
  }

  zoom(messages: Message[]){

    const items = [];

    messages.forEach(element => {
      items.push(element.attachment.url);
    });

    this.modalService.open('zoom', items);
  }

}
