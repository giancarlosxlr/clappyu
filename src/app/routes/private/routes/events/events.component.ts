import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  public markers = [];
  public events = [];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.markers = [];
    this.events = [];

    (async ()=>{
      var response = await this.userService.request('/events/opportunities','get');
      const arr = [];
      console.log(response.data.data);

      // response.data.data.forEach(element => {
      //   const key = Object.keys(element.locais)[0];
      //   arr.push({
      //     position:{
      //       latitude: element.locais[key].latitude,
      //       longitude: element.locais[key].longitude
      //     },
      //     icon: 'assets/images/point_event.png',
      //     title: element.nome_cliente,
      //   });
      // });
      // this.markers = arr;
      // this.events = response.data.data;
    })().catch((e)=>{
      console.log(e);
    })

  }








}
