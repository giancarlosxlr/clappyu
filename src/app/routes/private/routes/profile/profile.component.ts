import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../../../services/modal.service';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from '../../../../services/message.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public data;
  public token;
  public loader;
  public scrolled = false;
  public showCover = false;


  constructor(private modalService: ModalService, private userService: UserService,private router: Router, private route: ActivatedRoute, private messageService: MessageService) {
    
    this.route.params.subscribe((data: any)=>{
      this.token = data.token;
      this.update();
    });

  }

  ngOnInit() {

  }

  update(){
    this.showCover = false;
    this.loader = true;
    (async ()=>{
      const response = await this.userService.request(`/profile/${this.token}`, 'get');
      if(response.statusCode != 200)throw response.data;
      this.data = response.data.data;
      console.log(this.data);
    })().then(()=>{
      this.loader = false;
    }).catch(()=>{
      this.loader = false;
    });
  }


  get self(){
    if(!this.userService.data)return false;
    return this.token === this.userService.data.perfil.token;
  }

  updateProfile(){
    this.modalService.open('user-edit', {onUpdtate: ()=>{
       this.update();
    }});
  }

  scroll(e){
    this.scrolled = (e.target.offsetHeight + e.target.scrollTop) >= (e.target.scrollHeight - 5);
  }

  initChat(id_client: number){
    this.messageService.initChat(id_client).then((chatToken)=>{
      
      // this.router.navigate(['/private/chat/'])
    });
  }
}
