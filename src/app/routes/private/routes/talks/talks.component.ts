import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { MessageService } from '../../../../services/message.service';
import { Chat } from '../../../../classes/chat.class';
import { Router } from '@angular/router';

@Component({
  selector: 'app-talks',
  templateUrl: './talks.component.html',
  styleUrls: ['./talks.component.scss']
})
export class TalksComponent implements OnInit {

  public chats: Chat[] = [];

  constructor(private userService: UserService, private messageService: MessageService, private router: Router) { }

  ngOnInit() {
    this.chats = this.messageService.chats;
    this.messageService.change.subscribe(()=>{
      
    });
  }


  openChat(token){
    this.messageService.updateMessages(token).then(()=>{
      this.router.navigate(['/private/chat/', token]);
    });
  }

}
