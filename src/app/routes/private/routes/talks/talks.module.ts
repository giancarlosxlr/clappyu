import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TalksComponent } from './talks.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from '../../../../shared/shared.module';
import { TalkCardComponent } from './components/talk-card/talk-card.component';

@NgModule({
  declarations: [TalksComponent, TalkCardComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule
  ]
})
export class TalksModule { 
  public static route: Route = {
    path: 'talks',
    component: TalksComponent,
    data: {
      cache: true
    },
    resolve: [],
    children: []
  };
}
