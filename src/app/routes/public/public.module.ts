import { NgModule } from '@angular/core';
import { PublicComponent } from './public.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { MainModule } from './routes/main/main.module';
import { LoginModule } from './routes/login/login.module';
import { RegisterModule } from './routes/register/register.module';

@NgModule({
  declarations: [PublicComponent],
  imports: [
    SharedModule,
    RouterModule,
    MainModule,
    LoginModule,
    RegisterModule
  ]
})
export class PublicModule { 


  public static route: Route = {
    path: 'public',
    component: PublicComponent,
    resolve: [],
    children: [
      MainModule.route,
      LoginModule.route,
      RegisterModule.route
    ]
  };
}
