import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public disabled = false;
  public valid = false;

  constructor(private apiService: ApiService, private router: Router) {
    // this.apiService.request('/customer-types', {method: 'get'}).then((response)=>{
    //   console.log(response);
    //  });
  }

  ngOnInit() {
  }

  send(data) {
    this.disabled = true;
    this.apiService.request('/auth/sign-up', {
      method: 'POST',
      body: data
    }).then((response) => {
      console.log(response);
      if (response.statusCode === 200) {
        this.router.navigate(['/public/main']);
      }
      this.disabled = false;
    });

  }

}
