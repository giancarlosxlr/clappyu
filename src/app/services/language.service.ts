import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  private _lang = 'en';


  constructor() { }



  get language(){
    return this._lang;
  }

  async set(lang){
    this._lang = lang;
  }
}
