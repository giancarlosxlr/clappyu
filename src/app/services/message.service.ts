import { Injectable, EventEmitter } from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from '../../environments/environment';
import { UserService } from './user.service';
import { toBase64 } from '../helpers';
import { Chat } from '../classes/chat.class';
import { Message } from '../classes/message.class';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private _socket;
  private _tokenUsed;
  public chats: Chat[] = [];
  public change = new EventEmitter();

  constructor(private userService: UserService) {
    this.connect();
    this.userService.change.subscribe(() => {
      this.connect();
    });
  }


  private update(){
    var now = new Date().getTime();
    this.chats.forEach((chat)=>{
      chat.messages.sort((a,b)=>{
        return a.time.getTime() - b.time.getTime();
      });

      chat.messages.forEach((msg, index)=>{
        msg.childsAttachments = [];
        let i = index;
        let run = true;
        while(run){
          const item = chat.messages[i];
          if(item && item.from == msg.from && item.attachment){
            msg.childsAttachments.push(item);
          }else{
            run = false;
            break;
          }
          i++;
        }

        msg.childsAttachments.reverse();
      });

    });
    this.chats.sort((a,b)=>{
      return (b.lastMessage ? b.lastMessage.time.getTime() : now) - (a.lastMessage ? a.lastMessage.time.getTime() : now);
    });

    this.change.emit();
  }


  async updateMessages(token){
    const chat = this.chats.find((chat)=>{
      return chat.token == token;
    });
    if(!chat)return;

    if(chat.messages.length > 1) return;

    await this.loadMessages(chat, 20);
  }


  async loadMessages(chat: Chat, qtd) {

    const response = await this.userService.request(`/chats/${chat.token}/messages`, 'POST', { per_page: qtd });

    response.data.data.forEach(element => {
      const old = chat.messages.find((chat)=>{
        return chat.token == element.token;
      });

      if(!old){
        var msg = new Message(
          element.token,
          element.message,
          new Date(element.created_at),
          element.token_artista,
          element.avatar
        );
        if(element.attachments[0]){
          msg.attachment = {
            name: element.attachments[0].nome,
            url: element.attachments[0].document
          }
        }
        msg.status = 'sent';
        chat.messages.push(msg);
        response.data.data;
      }
    });

    this.update();
  }

  

  async sendMessage(token, text, attachment?) {
    const chat = this.chats.find((chat)=>{
      return chat.token == token;
    });

    if(!chat)return;

    const msg = new Message(
      null,
      text,
      new Date(new Date().toLocaleString('pt-BR', { timeZone: 'Europe/Lisbon' })),
      this.userService.data.perfil.token,
      this.userService.data.avatar
    );
    const formData = new FormData();
    formData.append('message', text);
    if (attachment) {
      const base64 = await toBase64(attachment);
      const url = URL.createObjectURL(attachment);

      formData.append('attachments[]', base64);
      formData.append('titles[]', attachment.name);
      msg.attachment = {
        name: attachment.name,
        url: url
      }
    }

    msg.status = 'sent';
    chat.messages.push(msg);
    this.update();


    this.userService.request(`/chats/${token}/messages/store`, 'POST', formData).then(()=>{
      msg.status = 'sent';
      this.update();
    }).catch(()=>{
      msg.status = 'error';
      this.update();
    })

  }


  getChat(token) {
    return this.chats.find((item) => {
      return item.token == token;
    });
  }


  connect() {
    if (!this.userService.isLogged) {
      this.disconnect();
      return;
    }
    if (this._socket && this._tokenUsed == this.userService.token) {
      return;
    }
    this.disconnect();
    this._tokenUsed = this.userService.token;
    this._socket = io(environment.apiURL, {
      // transportOptions: {
      //   polling: {
      //     extraHeaders: {
      //       Authorization: 'Bearer ' + this.userService.token
      //     }
      //   }
      // }
    });

    this.userService.request(`/chats`, 'get').then((response) => {
      response.data.data.forEach(element => {
        this.addChat(element);
      });
      this.update();
    });
  }

  disconnect() {
    if (this._socket) {
      this._socket.disconnect();
    }
    this.chats = [];
    this.update();
  }

  private addChat(item) {

    const find = this.chats.find((chat) => {
      return item.token == chat.token;
    });

    if (!find) {
      const chat = new Chat(
        item.token,
        item.estado,
        item.token_evento ? { token: item.token_evento } : null,
        item.unread
      );

      item.participants.forEach(participant => {
        if(this.userService.data.perfil.token != participant.token_artista){
          chat.participants.push({
            token: participant.token_artista,
            avatar: participant.avatar,
            type: participant.id_tipo,
            username: participant.user
          });
        }

      });
      if (item.lastMessage) {
        var msg = new Message(
          item.lastMessage.data.token,
          item.lastMessage.data.message,
          new Date(item.lastMessage.data.created_at),
          item.lastMessage.data.token_artista,
          item.lastMessage.data.avatar
        );
        if(item.lastMessage.data.attachments[0]){
          msg.attachment = {
            name: item.lastMessage.data.attachments[0].nome,
            url: item.lastMessage.data.attachments[0].document
          }
        }
        msg.status = 'sent';
        chat.messages.push(msg);
      }
      this.chats.push(chat);
    }
  }

  async initChat(id_cliente: number) {
    const response = await this.userService.request(`/chats/store`, 'POST', { id_cliente: id_cliente });
    this.addChat(response.data.data);
    this.update();
  }

}
