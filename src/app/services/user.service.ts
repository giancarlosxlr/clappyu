import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from './api.service';
import { toBase64 } from '../helpers';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public token = null;
  public data = null;
  public change = new EventEmitter();

  constructor(private api: ApiService) { 

  }

  async resolve(){
    var token = localStorage.getItem('token');
    if(token){
      this.token = token;
      const responseData = await this.request(`/account`, 'GET');
      this.data = responseData.data.data;
      console.log(this.data);

    }
  }

  get isLogged(){
    return this.token ? true : false;
  }

  async logout(){
    localStorage.removeItem('token');
    this.token = null;
    this.data = null;
    this.change.emit();

  }

  async edit(data){
    const formdata = new FormData();

    for (let key in data) {
      let val = data[key];
      if(key == 'cover' || key == 'avatar'){
        val = await toBase64( data[key]);
      }
      formdata.append(key, val);
    }

    var r = await this.request('/profile/update', 'POST', formdata);
    console.log(r);
    // const responseData = await this.request(`/account`, 'GET');
    // this.data = responseData.data.data;
  }

  async login(data){

    const response = await this.api.request('/auth/sign-in', {
      method: 'POST',
      body: data
    });
    
    if (response.statusCode === 200) {
      this.token = response.data.token;
      localStorage.setItem('token', this.token);
      const responseData = await this.request(`/account`, 'GET');
      this.data = responseData.data.data;
      this.change.emit();
    }else{
      throw 'Login Error';
    }

  }

  async request(url, method, body?){
      return await this.api.request(url, {
        Authorization: 'Bearer ' + this.token,
        method: method,
        body: body
      });
  }

}
