import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Formable } from '../../../classes/formable.class';

@Component({
  selector: 'app-image-change',
  templateUrl: './image-change.component.html',
  styleUrls: ['./image-change.component.scss']
})
export class ImageChangeComponent extends Formable {
  public preview;
  private _input;
  @Input() type = 'full';

  @ViewChild('input') set input(elm){
    this._input = elm.nativeElement;
    this._input.addEventListener('change', () => {
      this.value  = this._input.files[0];
      var reader  = new FileReader();

      reader.onloadend = ()=> {
        this.preview = reader.result;
      }

      if (this.value) {
        reader.readAsDataURL(this.value);
      } else {
        this.preview = null;
      }
    });
  };

  clear(){
    this._input.value = '';
    this.value = null;
    this.preview = null;
  }

  change(){
    this._input.click();
  }

}
