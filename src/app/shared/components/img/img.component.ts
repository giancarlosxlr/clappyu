import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'app-img',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.scss']
})
export class ImgComponent implements OnInit {

  static cache = [];

  @Input() type = 'cover';


  @HostBinding('attr.state') get state(){
      if(!this.item){
        return 'empty';
      }
      return this.item.state;
  };
  public item;

  @Input() set src(src) {
    if(!src){
      this.item = null;
      return;
    }

    this.item = ImgComponent.cache.find((item) => {
      return item.src == src;
    });

    if (!this.item) {
      this.item = {
        src: src,
        img: document.createElement('img'),
        state: 'load'
      };
      this.item.img.src = this.item.src;
      if (ImgComponent.cache.length > 99) {
        ImgComponent.cache.splice(0, 1);
      }
    } else {
      let index = ImgComponent.cache.indexOf(this.item);
      ImgComponent.cache.splice(index, 1);
    }

    ImgComponent.cache.push(this.item);
    if(this.item.state != 'load'){
      return;
    }

    this.item.img.addEventListener('load', () => {
      this.item.state = 'loaded';
    });
    this.item.img.addEventListener('error', () => {
      this.item.state = 'error';
    });

  }

  constructor() { 

  }

  ngOnInit() {

  }




}
