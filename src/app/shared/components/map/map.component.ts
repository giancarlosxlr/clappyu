import { Component, OnInit, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  public map: any;
  public loader = false;

  private _markers = [];
  @Input() set markers(markers: any[]) {
    
    this._markers.forEach((marker) => {
      marker.setMap(null);
    });
    this._markers = [];

    if(!markers)return;
    markers.forEach((item, i) => {

      const marker = new google.maps.Marker({
        position: new google.maps.LatLng(parseInt(item.position.latitude), parseInt(item.position.longitude)),
        map: this.map,
        icon: item.icon,
        title: item.title,
        zIndex: i + 1
      });

      marker.addListener('click', ()=> {
        this.map.setCenter(marker.getPosition());
      });

      this._markers.push(marker);
      
    });
    this.center();
  };

  constructor(private element: ElementRef) {

    this.map = new google.maps.Map(element.nativeElement, {
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      gestureHandling: 'cooperative',
      styles: [
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#f5f5f5"
            }
          ]
        },
        {
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#f5f5f5"
            }
          ]
        },
        {
          "featureType": "administrative",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "administrative",
          "elementType": "geometry",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#bdbdbd"
            }
          ]
        },
        {
          "featureType": "poi",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#eeeeee"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#e5e5e5"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#dadada"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        },
        {
          "featureType": "transit",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "transit.line",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#e5e5e5"
            }
          ]
        },
        {
          "featureType": "transit.station",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#eeeeee"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#c9c9c9"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        }
      ],
      disableDefaultUI: true
    });
  }


  center() {
    navigator.geolocation.getCurrentPosition((position) => {
      const center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      this.map.setCenter(center);


      var bounds = new google.maps.LatLngBounds();
      this.map.setCenter(center);



      var image = {
        url: 'assets/images/point_center.png',
        size: new google.maps.Size(92, 92),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(46, 46)
      };
      const centerMap = new google.maps.Marker({
        position: center,
        map: this.map,
        icon: image,
        title: 'me',
        zIndex: 0
      });

      bounds.extend(centerMap.getPosition());   

      this._markers.forEach((marker)=>{
        bounds.extend(marker.getPosition());
      });

      this.map.fitBounds(bounds);

      if(this._markers.length < 1){
        this.map.setZoom(15);
      }
      
    });
  }

  ngOnInit() {
    this.center();

    setTimeout(()=>{
      this.loader = false;
    });
  }

}
