import { Component, Input } from '@angular/core';
import { Formable } from '../../../classes/formable.class';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})
export class RadioComponent extends Formable {

  @Input() options = [];
}
