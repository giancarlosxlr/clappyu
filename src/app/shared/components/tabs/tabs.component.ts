import { Component, OnInit, ElementRef, OnDestroy, ViewChild, Input } from '@angular/core';
import * as Flickity from 'flickity';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnDestroy {

  @Input() type = 'footer';


  public slider: any;
  public element;
  public items = [];
  private timer;
  public options = {
    selectedAttraction: 0.3,
    friction: 0.8,
    cellAlign: 'center',
    pageDots: false,
    prevNextButtons: false,
    setGallerySize: false,
    percentPosition: true,
    contain: true,
    freeScroll: false,
    draggable: false
  };

@ViewChild('container') set setContainer(elm){
  if(!elm)return;
  if (this.slider) {
    this.slider.destroy();
  }
  this.slider = new Flickity(elm.nativeElement, this.options);
  this.slider.resize();
  this.updateVisual();
}

  private updateVisual;

  constructor() {
    this.updateVisual = () => {
      if (this.timer) {
        clearTimeout(this.timer);
      }
      this.timer = setTimeout(() => {
        if(this.slider)this.slider.resize();
      }, 1000);
    }
    window.addEventListener('resize', this.updateVisual);
  }



  get selectedIndex() {
    return this.slider.selectedIndex;
  }

  public append(tab) {
    const index = this.items.indexOf(tab);
    if (index < 0) {
      this.items.push(tab);
      this.slider.append(tab.element.nativeElement);
      this.slider.resize();
      this.updateVisual();
    }
  }

  remove(item) {
    const index = this.items.indexOf(item);
    if (index > -1) {
      this.items.splice(index, 1);
      this.slider.remove(item.element.nativeElement);
    }
  }

  select(index, isWrapped?, isInstant?) {
    this.slider.select(index, isWrapped, isInstant);
  }

  ngOnDestroy() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    window.removeEventListener('resize', this.updateVisual);
    if(this.slider)this.slider.destroy();
  }

}
